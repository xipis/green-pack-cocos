import { _decorator, Component, instantiate, Node } from 'cc';
import { PuzzleSprite } from '../puzzle/PuzzleSprite';
const { ccclass, property, executeInEditMode} = _decorator;

@ccclass('TestPuzzle')
@executeInEditMode(true)
export class TestPuzzle extends Component {
    @property({type:Node})
    public piece:Node;
    start(): void {
        if (!this.piece)
            return

        this.node.removeAllChildren();
        for (let c = 0; c < 10; c++) {
            for (let r = 0; r < 6; r++) {
                let idx = c + r * 10;
                if (idx < this.node.children.length)
                    continue;
                let node = instantiate(this.piece);
                this.node.addChild(node)
                // let node = this.node.children[idx]
                node.setPosition(c * 80, (6 - r) * 80, 0)
                node.getComponent(PuzzleSprite).columnMax = 10
                node.getComponent(PuzzleSprite).rowMax = 6
                node.getComponent(PuzzleSprite).column = c + 1
                node.getComponent(PuzzleSprite).row = r + 1
            }
        }
    }
}


