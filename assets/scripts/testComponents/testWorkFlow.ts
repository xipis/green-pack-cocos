import { _decorator, Component, Node, tween, v3 } from 'cc';
import { GPDrag } from '../components/GPDrag';
import { GPWorkFlowNode } from '../components/GPWorkFlow';
const { ccclass, property } = _decorator;

@ccclass('testWorkFlow')
export class testWorkFlow extends Component {
    start() {
        let drag = this.getComponent(GPDrag)
        let testNode = new GPWorkFlowNode();
        let self = this;
        testNode.OnStart = (wfNode:GPWorkFlowNode)=>{
            tween(self.node).by(0.05, {position:v3(10, 0, 0)})
                .by(0.1, {position:v3(-20, 0, 0)})
                .by(0.05, {position:v3(10, 0, 0)})
                .call(()=>{
                    wfNode.done()
                }).start()
        }
        drag.backHomeWorkFlow.insertHeadNode(testNode);
    }

    update(deltaTime: number) {
        
    }
}


