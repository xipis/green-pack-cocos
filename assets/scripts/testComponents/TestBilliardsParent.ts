import { _decorator, Component, Node, v3 } from 'cc';
import { TestBilliards } from './TestBilliards';
const { ccclass, property } = _decorator;

@ccclass('TestBilliardsParent')
export class TestBilliardsParent extends Component {
    start() {
        for (let i = 0; i < this.node.children.length; i++) {
            let ball:TestBilliards = this.node.children[i].getComponent(TestBilliards);
        
            ball.scrollSpeed = i % 4 + 1;
        
            let ax = i % 4;
            let ay = Math.floor(i / 4);
        
            let az = Math.sqrt(18 - ax * ax - ay * ay);
            let len = Math.sqrt(18);
        
            ball.scrollAxis = v3(ax / len, ay / len, az / len);
        }
    }

    update(deltaTime: number) {
        
    }
}


