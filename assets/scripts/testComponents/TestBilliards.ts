import { _decorator, Component, Mat4, Sprite, v3 } from 'cc';
const { ccclass, property } = _decorator;
// dynamicAtlasManager.enabled = false;
@ccclass('TestBilliards')
export class TestBilliards extends Component {
    @property
    public scrollAxis = v3(1, 0, 0)

    private matrix = new Mat4();
    private mat = [        
        1, 0, 0,
        0, 1, 0,
        0, 0, 1]

    @property
    public scrollSpeed = 0;

    start() {

    }
    scroll(rad, x, y, z) {

        // 矩阵旋转
        // 传入的是逆矩阵，所以计算反向旋转
        // 因为只需要计算3d旋转，所以用3x3矩阵表示
        const s = Math.sin(-rad);
        const c = Math.cos(-rad);
        const t = 1 - c;


        let a = this.mat;
        const a00 = a[0]; const a01 = a[1]; const a02 = a[2];
        const a10 = a[3]; const a11 = a[4]; const a12 = a[5];
        const a20 = a[6]; const a21 = a[7]; const a22 = a[8];

        const b00 = x * x * t + c; const b01 = y * x * t + z * s; const b02 = z * x * t - y * s;
        const b10 = x * y * t - z * s; const b11 = y * y * t + c; const b12 = z * y * t + x * s;
        const b20 = x * z * t + y * s; const b21 = y * z * t - x * s; const b22 = z * z * t + c;

        a[0] = a00 * b00 + a10 * b01 + a20 * b02;
        a[1] = a01 * b00 + a11 * b01 + a21 * b02;
        a[2] = a02 * b00 + a12 * b01 + a22 * b02;

        a[3] = a00 * b10 + a10 * b11 + a20 * b12;
        a[4] = a01 * b10 + a11 * b11 + a21 * b12;
        a[5] = a02 * b10 + a12 * b11 + a22 * b12;

        a[6] = a00 * b20 + a10 * b21 + a20 * b22;
        a[7] = a01 * b20 + a11 * b21 + a21 * b22;
        a[8] = a02 * b20 + a12 * b21 + a22 * b22;

    }
    update(dt) {
        let rad = dt * this.scrollSpeed;
        this.scroll(rad, this.scrollAxis.x, this.scrollAxis.y, this.scrollAxis.z);
        let a = this.mat
        this.matrix.set(            
            a[0], a[1], a[2], 0,
            a[3], a[4], a[5], 0,
            a[6], a[7], a[8], 0,
            0, 0, 0, 0,)
        this.getComponent(Sprite).material.setProperty("b_matrix", this.matrix)
    }
}


