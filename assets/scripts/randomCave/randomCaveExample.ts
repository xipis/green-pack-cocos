import { _decorator, Color, Component, instantiate, Node, Sprite, v3 } from 'cc';
import { RandomCaveGenerator } from './RandomCaveGenerator';
const { ccclass, property } = _decorator;

@ccclass('randomCaveExample')
export class randomCaveExample extends Component {
    @property({type:Node})
    public cell:Node
    private generator = new RandomCaveGenerator()
    private nodes:Node[] = []
    start() {
        this.generator.init();
        let startX = -this.generator.columns / 2 * 10
        let startY = -this.generator.rows / 2 * 10
        for (let i = 0; i < this.generator.columns; i++) {
            for (let j = 0; j < this.generator.rows; j++) {
                let node = instantiate(this.cell);
                this.node.addChild(node)
                node.setPosition(v3(i * 10 + startX, j * 10 + startY, 0))
                if (this.generator.isWall(i, j))
                node.getComponent(Sprite).color = Color.GRAY
                this.nodes[i + j * this.generator.rows] = node;
            }
        }
    }

    public randomInit() {
        this.generator.init();
        this.apply()
    }

    public step() {
        this.generator.step();
        this.apply()
    }
    private apply() {
        let startX = -this.generator.columns / 2 * 10
        let startY = -this.generator.rows / 2 * 10
        for (let i = 0; i < this.generator.columns; i++) {
            for (let j = 0; j < this.generator.rows; j++) {
                let node = this.nodes[i + j * this.generator.rows]
                node.setPosition(v3(i * 10 + startX, j * 10 + startY, 0))
                node.getComponent(Sprite).color = this.generator.isWall(i, j) ? Color.GRAY: Color.WHITE
            }
        }
    }
}


