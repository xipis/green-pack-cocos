import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

export class RandomCaveGenerator {
    public columns = 30
    public rows = 35
    public data:boolean[] = []
    private cache:boolean[] = []

    public initRate = 0.5;
    public init() {
        for (let i = 0; i < this.columns * this.rows; i++) {
            this.data[i] = Math.random() < this.initRate;
        }
    }
    public isWall(c: number, r: number) {
        let idx = c + r * this.columns
        if (idx < 0 || idx >= this.columns * this.rows)
            return true
        return this.data[idx]
    }
    public count(c: number, r: number, n: number) {
        let count = 0;
        for (let i = -n; i <= n; i++) {
            for (let j = -n; j <= n; j++) {
                if (this.isWall(c + i, r + j)) {
                    count++
                }
            }
        }
        return count
    }
    public setCache(c:number, r:number, b:boolean) {
        this.cache[c + r * this.columns] = b;
    }
    public step() {
        for (let c = 0; c < this.columns; c++) {
            for (let r = 0; r < this.rows; r++) {
                let count = this.count(c, r, 1)
                let count2 = this.count(c, r, 2)
                this.setCache(c, r, count >= 5 || count2 <= 2)
            }
        }
        this.data = Array.from(this.cache)
    }
}


